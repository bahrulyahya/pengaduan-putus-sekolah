from rest_framework import serializers
from django.utils.translation import gettext as _

from pengaduan.models import Pengaduan


class PengaduanSerializer(serializers.ModelSerializer):
    alamat = serializers.SerializerMethodField("get_alamat")
    ttl = serializers.SerializerMethodField("get_ttl")

    class Meta:
        model = Pengaduan
        fields = ("nama", "alamat", "ttl")

    def get_alamat(self, obj):
        return obj.get_alamat_lengkap()

    def get_ttl(self, obj):
        FULL_BULAN = {
            1: "Januari",
            2: "Februari",
            3: "Maret",
            4: "April",
            5: "Mei",
            6: "Juni",
            7: "Juli",
            8: "Agustus",
            9: "September",
            10: "Oktober",
            11: "November",
            12: "Desember",
        }
        if obj.tanggal_lahir and obj.tempat_lahir:
            text_tanggal_lahir = "{} {} {}".format(
                obj.tanggal_lahir.day,
                FULL_BULAN[obj.tanggal_lahir.month],
                obj.tanggal_lahir.year,
            )

            return "{}, {}".format(obj.tempat_lahir, text_tanggal_lahir)
        return "---"
