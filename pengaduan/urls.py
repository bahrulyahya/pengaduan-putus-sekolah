from django.urls import include, path
from rest_framework.routers import DefaultRouter

from pengaduan.viewset import PengaduanViewset

router = DefaultRouter()
router.register(r"pengaduan/list-pengaduan", PengaduanViewset)

urlpatterns = [
    path("", include(router.urls)),
]
