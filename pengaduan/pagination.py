import math

from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.utils.urls import remove_query_param, replace_query_param


class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        def get_next_link(self):
            if not self.page.has_next():
                return None
            url = self.request.get_full_path()
            page_number = self.page.next_page_number()
            # print(self.paginate_queryset)
            return replace_query_param(url, self.page_query_param, page_number)

        def get_previous_link(self):
            if not self.page.has_previous():
                return None
            url = self.request.get_full_path()
            page_number = self.page.previous_page_number()
            if page_number == 1:
                return remove_query_param(url, self.page_query_param)
            return replace_query_param(url, self.page_query_param, page_number)

        return Response(
            {
                # "path_next": get_next_link(self),
                # "path_previous": get_previous_link(self),
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                # 'current_page': int(self.request.query_params.get('page', 1)),
                "count": self.page.paginator.count,
                # 'per_page': self.page_size,
                "total_pages": math.ceil(
                    round(self.page.paginator.count / self.page_size, 1)
                ),
                "results": data,
            }
        )
