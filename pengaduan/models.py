from django.db import models
from master.utils import AtributTambahan, get_no_aduan
from master.models import Desa, Kelas, MasterKeterangan, Kecamatan


class Pengaduan(AtributTambahan):
    class Paket(models.IntegerChoices):
        PAKET_A = 1, "Paket A (SD)"
        PAKET_B = 2, "Paket B (SMP/MTs)"
        PAKET_C = 3, "Paket C (SMA/SMK/MA)"

    class JenisKelamin(models.IntegerChoices):
        LAKI_LAKI = 1, "Laki - Laki"
        PEREMPUAN = 2, "Perempuan"

    nomor_aduan = models.CharField(
        max_length=255, verbose_name="Nomor Aduan", null=True, blank=True
    )
    nomor_nik = models.CharField(
        max_length=255, verbose_name="Nomor NIK", null=True, blank=True
    )
    nama = models.CharField(max_length=255, verbose_name="Nama", null=True, blank=True)
    tempat_lahir = models.CharField(
        max_length=255, verbose_name="Tempat Lahir", null=True, blank=True
    )
    jenis_kelamin = models.IntegerField(
        choices=JenisKelamin.choices, null=True, blank=True
    )
    tanggal_lahir = models.DateField(null=True, blank=True)
    usia = models.IntegerField(default=0)
    pendidikan_terakhir = models.IntegerField(
        choices=Kelas.Jenjang.choices,
        null=True,
        blank=True,
        verbose_name="Pendidikan Terakhir / DO",
    )
    kelas = models.ForeignKey(Kelas, on_delete=models.SET_NULL, null=True, blank=True)
    keterangan = models.ForeignKey(
        MasterKeterangan, on_delete=models.SET_NULL, null=True, blank=True
    )
    alasan = models.CharField(
        max_length=255, verbose_name="Alasan", null=True, blank=True
    )

    desa = models.ForeignKey(Desa, on_delete=models.SET_NULL, null=True)
    alamat_lengkap = models.TextField(verbose_name="Alamat", null=True, blank=True)
    dusun = models.CharField(max_length=255, null=True, blank=True)
    rt = models.CharField(max_length=255, null=True, blank=True)
    rw = models.CharField(max_length=255, null=True, blank=True)

    desa_domisili = models.ForeignKey(
        Desa, on_delete=models.SET_NULL, null=True, related_name="desa_domisili"
    )
    alamat_lengkap_domisili = models.TextField(
        verbose_name="Alamat Domisili", null=True, blank=True
    )
    dusun_domisili = models.CharField(max_length=255, null=True, blank=True)
    rt_domisili = models.CharField(max_length=255, null=True, blank=True)
    rw_domisili = models.CharField(max_length=255, null=True, blank=True)

    nomor_nik_ayah = models.CharField(
        max_length=255, verbose_name="Nomor NIK Ayah", null=True, blank=True
    )
    nama_ayah = models.CharField(
        max_length=255, verbose_name="Nama Ayah", null=True, blank=True
    )
    nomor_nik_ibu = models.CharField(
        max_length=255, verbose_name="Nomor NIK Ibu", null=True, blank=True
    )
    nama_ibu = models.CharField(
        max_length=255, verbose_name="Nama Ibu", null=True, blank=True
    )

    keterangan_tolak = models.CharField(
        max_length=255, verbose_name="Keterangan Tolak", null=True, blank=True
    )
    nomor_hp = models.BigIntegerField(default=0)
    is_daftar = models.BooleanField(
        default=False, verbose_name="Apakah Ingin Mendaftar di PKBM?"
    )
    is_taken = models.BooleanField(
        default=False, verbose_name="Pengaduan Sudah Diambil"
    )
    taken_by = models.ForeignKey(
        "account.Account",
        related_name="%(app_label)s_%(class)s_taken_by_user",
        verbose_name="Diambil Oleh",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    kecamatan_sekolah = models.ForeignKey(
        Kecamatan, on_delete=models.SET_NULL, null=True, related_name="kecamatan_sekolah", blank=True
    )
    sekolah_tujuan = models.ForeignKey(
        "master.Sekolah",
        verbose_name="Tujuan Sekolah",
        related_name="%(app_label)s_%(class)s_tujuan_sekolah",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    sekolah = models.ForeignKey(
        "master.Sekolah", on_delete=models.SET_NULL, null=True, blank=True
    )
    paket = models.IntegerField(choices=Paket.choices, null=True, blank=True)

    def __str__(self):
        return self.nomor_aduan

    def save(self, *kwargs, **args):
        if not self.id:
            self.nomor_aduan = get_no_aduan(self)
        return super().save(*kwargs, **args)

    def get_alamat_lengkap(self):
        if self.desa:
            return "{} RT{}/RW{}, Desa {}, Kec. {}, Kab. Kediri".format(
                self.alamat_lengkap,
                self.rt,
                self.rw,
                self.desa.nama,
                self.desa.kecamatan.nama,
            )
        return self.alamat_lengkap

    def get_alamat_lengkap_domisili(self):
        if self.desa_domisili:
            return "{} RT{}/RW{}, Desa {}, Kec. {}, Kab. Kediri".format(
                self.alamat_lengkap_domisili,
                self.rt_domisili,
                self.rw_domisili,
                self.desa_domisili.nama,
                self.desa_domisili.kecamatan.nama,
            )
        return self.alamat_lengkap

    class Meta:
        verbose_name = "Pengaduan"
        verbose_name_plural = "Pengaduan"


class RiwayatPengaduan(AtributTambahan):
    pengaduan = models.ForeignKey(Pengaduan, on_delete=models.CASCADE, null=True)
    keterangan = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.pengaduan.nomor_aduan, self.keterangan)

    class Meta:
        verbose_name = "Riwayat Pengaduan"
        verbose_name_plural = "Riwayat Pengaduan"
