from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from django.shortcuts import get_object_or_404
from pengaduan.pagination import CustomPagination
from master.models import Sekolah
from pengaduan.models import Pengaduan
from pengaduan.serializers import PengaduanSerializer


class PengaduanViewset(viewsets.ModelViewSet):
    queryset = Pengaduan.objects.all()
    serializer_class = PengaduanSerializer
    filter_backends = [DjangoFilterBackend]
    pagination_class = CustomPagination
    http_method_names = [
        "get",
    ]

    def get_queryset(self):
        qs = super().get_queryset()
        # print(dir(self.request))
        sekolah = self.request.query_params["sekolah"]
        if sekolah != "":
            sekolah_obj = get_object_or_404(Sekolah, id=sekolah)
            if sekolah_obj:
                qs = qs.filter(sekolah=sekolah_obj)
        return qs.order_by("nama")
