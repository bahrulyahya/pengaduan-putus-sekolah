from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe

from account.admin import MasyarakatCreation
from account.models import Petugas
from master.models import Pengaturan
from master.utils import file_is_exists
from pengaduan.admin import PengaduanForm
from pengaduan.models import Pengaduan

# Create your views here.


@login_required(login_url=reverse_lazy("admin:login"))
def dashboard(request, extra_context={}):
    petugas = Petugas.objects.all().count()
    pengaduan = Pengaduan.objects.all()
    semua = Pengaduan.objects.filter(~Q(status__in=[3, 1]))
    if (
        hasattr(request.user, "petugas")
        or hasattr(request.user, "adminsekolah")
        or hasattr(request.user, "masyarakat")
    ):
        pengaduan = pengaduan.filter(created_by=request.user)
        semua = semua.filter(created_by=request.user)
    masuk = pengaduan.filter(status=2).count()
    proses = pengaduan.filter(status=5).count()
    tolak = pengaduan.filter(status=4).count()
    selesai = pengaduan.filter(status=1).count()

    extra_context.update(
        {
            "title": "Dashboard",
            "petugas": petugas,
            "semua": semua.count(),
            "masuk": masuk,
            "proses": proses,
            "tolak": tolak,
            "selesai": selesai,
            "is_petugas": hasattr(request.user, "petugas")
            or hasattr(request.user, "adminsekolah"),
        }
    )
    return render(request, "admin/pengaduan/dashboard.html", extra_context)


def profil_sekolah(request, extra_context={}):
    field = [
        "kecamatan",
        "desa",
        "alamat",
        "email",
        "status_penyelenggara",
    ]
    extra_context.update(
        {"title": "Sistem Informasi Anak Putus Sekolah Bagi Masyarakat yang Membutuhkan Pendidikan", "field": field}
    )
    return render(request, "frontend/profil_sekolah.html", extra_context)


def frontend(request, extra_context={}):
    setting = Pengaturan.get_solo()
    gambar_header = setting.header_frontend
    isi = setting.isi
    if gambar_header and not file_is_exists(gambar_header.path):
        gambar_header = None
    extra_context.update(
        {
            "title": "Sistem Informasi Anak Putus Sekolah Bagi Masyarakat yang Membutuhkan Pendidikan",
            "forms": PengaduanForm(),
            "gambar_header": gambar_header,
            "isi": isi,
        }
    )
    return render(request, "frontend/index.html", extra_context)


def register(request, extra_context={}):
    register = MasyarakatCreation()
    if request.POST:
        register = MasyarakatCreation(request.POST)
        if register.is_valid():
            register.save()
            register = MasyarakatCreation()
            messages.add_message(
                request,
                messages.SUCCESS,
                mark_safe(
                    'Berhasil Register<br>Silahkan login di <a href="{}">sini</a>'.format(
                        reverse_lazy("admin:login")
                    )
                ),
            )
        else:
            messages.add_message(
                request, messages.ERROR, "Silahkan Perbaiki Kesalahan Dibawah"
            )
    extra_context.update(
        {
            "title": "Sistem Informasi Anak Putus Sekolah Bagi Masyarakat yang Membutuhkan Pendidikan",
            "forms": register,
        }
    )
    return render(request, "frontend/register.html", extra_context)
