from django import forms
from django.contrib import admin, messages
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import resolve, reverse, reverse_lazy
from django.utils.safestring import mark_safe

from pengaduan.models import Pengaduan, RiwayatPengaduan
from master.models import Kecamatan, Desa, Kelas, MasterKeterangan, Sekolah

# Register your models here.


class PengaduanForm(forms.ModelForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    kecamatan_domisili = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this),'domisili')"}),
    )
    pendidikan_terakhir = forms.ChoiceField(
        choices=[("", "---------")] + Kelas.Jenjang.choices,
        required=False,
        widget=forms.Select(
            attrs={"onChange": "getKelas($('#id_pendidikan_terakhir').val())"}
        ),
    )
    kelas = forms.ModelChoiceField(
        required=False,
        queryset=Kelas.objects.none(),
        widget=forms.Select(),
    )
    keterangan = forms.ModelChoiceField(
        required=False,
        queryset=MasterKeterangan.objects.all(),
        widget=forms.Select(),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)
    kecamatan_sekolah = forms.ModelChoiceField(
        queryset=Kecamatan.objects.all(),
        required=False,
        widget=forms.Select(
            attrs={"onChange": "getSekolah($(this).val())"}
        ),
    )
    desa_domisili = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)
    sekolah_tujuan = forms.ModelChoiceField(
        queryset=Sekolah.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(PengaduanForm, self).__init__(*args, **kwargs)
        choices = self.fields["pendidikan_terakhir"].choices
        del choices[3]
        ALASAN = [('kerja', "Kerja"), ('sibuk', "Sibuk"), ('tidak_punya_biaya', "Tidak Punya Biaya")]
        
        self.fields["pendidikan_terakhir"].choices = choices
        self.fields["alasan"] = forms.ChoiceField(choices=[("", "---------")]+ALASAN, required=False, widget=forms.Select())
        
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        self.fields["kecamatan_domisili"].to_field_name = "kode"
        self.fields["kecamatan_sekolah"].to_field_name = "kode"
        self.fields["desa_domisili"].to_field_name = "kode"
        # print(self.fields)
        self.fields["sekolah_tujuan"].label = "PKBM Tujuan"
        # if "nama" in self.fields:
        self.fields["nama"].label = "Nama Lengkap"
        # if "alamat_lengkap" in self.fields:
        self.fields["alamat_lengkap"].label = "Jalan"
        # if "alamat_lengkap_domisili" in self.fields:
        self.fields["alamat_lengkap_domisili"].label = "Jalan"

        self.fields["rt"].label = "RT"
        self.fields["rt_domisili"].label = "RT"

        self.fields["rw"].label = "RW"
        self.fields["rw_domisili"].label = "RW"

        self.fields["dusun_domisili"].label = "Dusun"

        self.fields["desa_domisili"].label = "Desa"
        self.fields["kecamatan_domisili"].label = "Kecamatan"
        self.fields["kecamatan_sekolah"].label = "Kecamatan"

        if self.instance.pk:
            if self.instance.desa:
                self.fields["kecamatan"].initial = self.instance.desa.kecamatan
            if self.instance.pendidikan_terakhir:
                self.fields["kelas"].queryset = Kelas.objects.filter(
                    jenjang=self.instance.pendidikan_terakhir
                )
            if self.instance.sekolah_tujuan:
                self.fields["sekolah_tujuan"].queryset = Sekolah.objects.filter(
                    id=self.instance.sekolah_tujuan.id
                )
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

        if "kecamatan_domisili" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan_domisili"))
                self.fields["kecamatan_domisili"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa_domisili" in self.data:
            try:
                des_pk = int(self.data.get("desa_domisili"))
                self.fields["desa_domisili"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

        if "kelas" in self.data:
            try:
                kelas_pk = int(self.data.get("kelas"))
                self.fields["kelas"].queryset = Kelas.objects.filter(pk=kelas_pk)
            except (ValueError, TypeError):
                pass

        if "sekolah_tujuan" in self.data:
            try:
                sekolah_pk = int(self.data.get("sekolah_tujuan"))
                self.fields["sekolah_tujuan"].queryset = Sekolah.objects.filter(
                    pk=sekolah_pk
                )
            except (ValueError, TypeError):
                pass

    def clean_nomor_nik(self):
        nomor_nik = self.data.get("nomor_nik")
        if not self.instance.pk:
            if nomor_nik.isnumeric():
                if len(nomor_nik) != 16:
                    raise forms.ValidationError(
                        "Nomor NIK: {} Tidak valid, Harus 16 Digit".format(nomor_nik)
                    )
            else:
                raise forms.ValidationError(
                    "Nomor NIK: {} Tidak valid, harus berisi angka".format(nomor_nik)
                )
        return nomor_nik

    def clean_nomor_nik_ayah(self):
        nomor_nik = self.data.get("nomor_nik_ayah")
        nomor_nik = nomor_nik.strip()
        if nomor_nik.isnumeric():
            if len(nomor_nik) != 16:
                raise forms.ValidationError(
                    "Nomor NIK: {} Tidak valid, Harus 16 Digit".format(nomor_nik)
                )
        else:
            raise forms.ValidationError(
                "Nomor NIK: {} Tidak valid, harus berisi angka".format(nomor_nik)
            )
        return nomor_nik

    def clean_nomor_nik_ibu(self):
        nomor_nik = self.data.get("nomor_nik_ibu")
        nomor_nik = nomor_nik.strip()
        if nomor_nik.isnumeric():
            if len(nomor_nik) != 16:
                raise forms.ValidationError(
                    "Nomor NIK: {} Tidak valid, Harus 16 Digit".format(nomor_nik)
                )
        else:
            raise forms.ValidationError(
                "Nomor NIK: {} Tidak valid, harus berisi angka".format(nomor_nik)
            )
        return nomor_nik

    field_order = [
        "nomor_nik",
        "nama",
        "kecamatan",
        "desa",
        "dusun",
        "rt",
        "rw",
        "alamat_lengkap",
        "kecamatan_domisili",
        "desa_domisili",
        "dusun_dusun",
        "rt_domisili",
        "rw_domisili",
        "alamat_lengkap_domisili",
        "tempat_lahir",
        "tanggal_lahir",
        "usia",
        "pendidikan_terakhir",
        "kelas",
        "keterangan",
        "nomor_nik_ayah",
        "nama_ayah",
        "nomor_nik_ibu",
        "nama_ibu",
    ]

    class Meta:
        model = Pengaduan
        exclude = ("status", "keterangan_tolak", "created_by", "nomor_aduan")


def proses_act(model_admin, request, queryset):
    if queryset:
        for data in queryset:
            data.status = 5
            data.save()
            RiwayatPengaduan.objects.create(
                pengaduan=data,
                keterangan="Pengaduan diproses",
                created_by=request.user,
            )
        messages.success(
            request, ("Berhasil Memproses {} Data Pengaduan".format(queryset.count()))
        )
    else:
        messages.error(request, ("belum memilih data Pengaduan"))


proses_act.short_description = "Proses Pengaduan yang dipilih"


def tolak_act(model_admin, request, queryset):
    if queryset:
        keterangan_tolak = request.POST.get("keterangan_tolak", None)
        for data in queryset:
            data.keterangan_tolak = keterangan_tolak
            data.status = 4
            data.save()
            RiwayatPengaduan.objects.create(
                pengaduan=data,
                keterangan=f"Pengaduan ditolak {keterangan_tolak}",
                created_by=request.user,
            )
        messages.success(
            request, ("Berhasil Menolak {} Data Pengaduan".format(queryset.count()))
        )
    else:
        messages.error(request, ("belum memilih data Pengaduan"))


tolak_act.short_description = "Tolak Pengaduan yang dipilih"


def setsekolah_act(model_admin, request, queryset):
    if queryset:
        sekolah = request.POST.get("sekolah", None)
        paket = request.POST.get("paket", None)
        sekolah_obj = get_object_or_404(Sekolah, pk=sekolah)
        for data in queryset:
            data.sekolah = sekolah_obj
            data.paket = paket
            data.status = 1
            data.keterangan_tolak = None
            data.save()
            RiwayatPengaduan.objects.create(
                pengaduan=data,
                keterangan=f"Pengaduan diproses di Sekolah {data.sekolah.nama_sekolah} {data.paket}",
                created_by=request.user,
            )
            RiwayatPengaduan.objects.create(
                pengaduan=data,
                keterangan="Pengaduan diselesai",
                created_by=request.user,
            )
            update_sekolah(sekolah_obj.id)
        messages.success(
            request, ("Berhasil Set Sekolah {} Data Pengaduan".format(queryset.count()))
        )
    else:
        messages.error(request, ("belum memilih data Pengaduan"))


setsekolah_act.short_description = "Selesai Pengaduan yang dipilih"


def kembalikan_pengaduan(model_admin, request, queryset):
    for data in queryset:
        sekolah = data.sekolah
        data.status = 2
        data.is_taken = False
        data.taken_by = None
        data.sekolah = None
        data.paket = None
        data.keterangan_tolak = None
        data.save()
        RiwayatPengaduan.objects.create(
            pengaduan=data,
            keterangan="Pengaduan dikembalikan",
            created_by=request.user,
        )
        update_sekolah(sekolah.id)
    messages.success(
        request, ("Berhasil Mengembalika {} Data Pengaduan".format(queryset.count()))
    )


kembalikan_pengaduan.short_description = "Kembalikan Pengaduan yang dipilih"

# def selesai_act(model_admin, request, queryset):
#     if queryset:
#         for data in queryset:
#             data.keterangan_tolak = None
#             data.status = 1
#             data.save()
#             RiwayatPengaduan.objects.create(
#                 pengaduan=data,
#                 keterangan="Pengaduan diselesai",
#                 created_by=request.user,
#             )
#         messages.success(
#             request,
#             ("Berhasil Menselesaikan {} Data Pengaduan".format(queryset.count())),
#         )
#     else:
#         messages.error(request, ("belum memilih data Pengaduan"))


# selesai_act.short_description = "Selesai Pengaduan yang dipilih"


def ambil_act(model_admin, request, queryset):
    if queryset:
        if queryset.filter(is_taken=True):
            html = """
            <table class="table table-bordered table-hover">
            <thead>
                <tr class="thead-dark">
                    <th>No</th>
                    <th>Pengaduan</th>
                    <th>Diambil Oleh</th>
                </tr>
            </thead>
            <tbody>

            """
            for index, i in enumerate(queryset.filter(is_taken=True)):
                html += """
                <tr>
                    <td class="table-light">{}</td>
                    <td class="table-light">{}</td>
                    <td class="table-light">{}</td>
                </tr>
                """.format(
                    index + 1,
                    i.nomor_aduan,
                    i.taken_by,
                )
            html += """
                </tbody>
            </table>
            Pengaduan tersebut tidak dapat diambil, karena sudah di ambil
            """
            messages.error(
                request,
                mark_safe(html),
            )
        else:
            for data in queryset:
                data.is_taken = True
                data.taken_by = request.user
                data.status = 2
                data.save()
                RiwayatPengaduan.objects.create(
                    pengaduan=data,
                    keterangan="Pengaduan diambil oleh {}".format(
                        request.user.nama_lengkap
                    ),
                    created_by=request.user,
                )
            messages.success(
                request,
                ("Berhasil Mengambil {} Data Pengaduan".format(queryset.count())),
            )
    else:
        messages.error(request, ("belum memilih data Pengaduan"))


ambil_act.short_description = "Ambil Pengaduan yang dipilih"


def update_sekolah(sekolah_id):
    sekolah_obj = get_object_or_404(Sekolah, pk=sekolah_id)
    verified_pengaduan = Pengaduan.objects.filter(status=1, sekolah=sekolah_obj)
    sekolah_obj.jumlah_paket_a = verified_pengaduan.filter(
        paket=Pengaduan.Paket.PAKET_A
    ).count()
    sekolah_obj.jumlah_paket_b = verified_pengaduan.filter(
        paket=Pengaduan.Paket.PAKET_B
    ).count()
    sekolah_obj.jumlah_paket_c = verified_pengaduan.filter(
        paket=Pengaduan.Paket.PAKET_C
    ).count()

    sekolah_obj.jumlah_laki_laki = verified_pengaduan.filter(
        jenis_kelamin=Pengaduan.JenisKelamin.LAKI_LAKI
    ).count()
    sekolah_obj.jumlah_perempuan = verified_pengaduan.filter(
        jenis_kelamin=Pengaduan.JenisKelamin.PEREMPUAN
    ).count()
    sekolah_obj.save()


class KecamatanFilter(admin.SimpleListFilter):
    title = "Kecamatan"
    parameter_name = "kecamatan"

    def lookups(self, request, model_admin):
        unitkerja_list = Kecamatan.objects.filter(status=1).values_list("pk", "nama")
        return list(unitkerja_list)

    def queryset(self, request, queryset):
        if self.value() != "" and self.value():
            return queryset.filter(desa__kecamatan=self.value())
        return queryset


class PengaduanAdmin(admin.ModelAdmin):
    list_display = (
        "nomor_aduan",
        "nomor_nik",
        "nama",
        "get_kecamatan",
        "sekolah_tujuan",
        "keterangan",
        "get_created_at",
    )
    form = PengaduanForm
    search_fields = ("nomor_aduan", "nomor_nik", "nama")
    list_filter = [
        KecamatanFilter,
    ]

    def get_form(self, request, obj=None, **kwargs):
        form = super(PengaduanAdmin, self).get_form(request, obj, **kwargs)
        form.request = request
        return form

    def has_change_permission(self, request, obj=None):
        # if obj:
        #     if obj.status == 1 or obj.status == 4:
        #         return False
        return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def get_readonly_fields(self, request, obj):
        readonly_fields = ("nomor_aduan",)
        if obj:
            readonly_fields = ("nomor_aduan", "nomor_nik")
        return readonly_fields

    def get_created_at(self, obj):
        return obj.created_at

    get_created_at.short_description = "Dibuat Pada"

    def get_kecamatan(self, obj):
        if obj.desa:
            return obj.desa.kecamatan
        return "-"

    get_kecamatan.short_description = "Kecamatan"

    def get_aksi(self, obj):
        html = """
            <a href="{}" target="_blank" class="btn btn-success btn-sm" >
                <i class="fas fa-print" ></i>
                Cetak Pernyataan
            </a>
            """.format(
            reverse_lazy(
                "admin:pengaduan_pengaduan_cetak_pernyataan", kwargs={"id": obj.id}
            )
        )
        return mark_safe(html)

    get_aksi.short_description = "Aksi"

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (
                "Idenditas Siswa",
                {
                    "fields": (
                        "nomor_nik",
                        "nama",
                    ),
                },
            ),
            (
                "Alamat Sesuai KTP",
                {
                    "fields": (
                        "kecamatan",
                        "desa",
                        "dusun",
                        "rt",
                        "rw",
                        "alamat_lengkap",
                    ),
                },
            ),
            (
                "Alamat Sesuai Domisili",
                {
                    "fields": (
                        "kecamatan_domisili",
                        "desa_domisili",
                        "dusun_domisili",
                        "rt_domisili",
                        "rw_domisili",
                        "alamat_lengkap_domisili",
                    ),
                },
            ),
            (
                "Idenditas Siswa",
                {
                    "fields": (
                        "tempat_lahir",
                        "tanggal_lahir",
                        "jenis_kelamin",
                        "nomor_hp",
                        "usia",
                        "pendidikan_terakhir",
                        "keterangan",
                        "kelas",
                        "is_daftar",
                        "kecamatan_sekolah",
                        "sekolah_tujuan",
                        "alasan",
                    ),
                },
            ),
            ("Identitas PKBM", {"fields": ()}),
            (
                "Idenditas Orang Tua",
                {
                    "fields": (
                        "nomor_nik_ayah",
                        "nama_ayah",
                        "nomor_nik_ibu",
                        "nama_ibu",
                    ),
                },
            ),
        )
        if obj:
            fieldsets = (
                (
                    None,
                    {
                        "fields": ("nomor_aduan",),
                    },
                ),
                (
                    "Idenditas Siswa",
                    {
                        "fields": (
                            "nomor_nik",
                            "nama",
                        ),
                    },
                ),
                (
                    "Alamat Sesuai KTP",
                    {
                        "fields": (
                            "kecamatan",
                            "desa",
                            "dusun",
                            "rt",
                            "rw",
                            "alamat_lengkap",
                        ),
                    },
                ),
                (
                    "Alamat Sesuai Domisili",
                    {
                        "fields": (
                            "kecamatan_domisili",
                            "desa_domisili",
                            "dusun_domisili",
                            "rt_domisili",
                            "rw_domisili",
                            "alamat_lengkap_domisili",
                        ),
                    },
                ),
                (
                    "Idenditas Siswa",
                    {
                        "fields": (
                            "tempat_lahir",
                            "tanggal_lahir",
                            "jenis_kelamin",
                            "nomor_hp",
                            "usia",
                            "pendidikan_terakhir",
                            "keterangan",
                            "kelas",
                            "is_daftar",
                            "kecamatan_sekolah",
                            "sekolah_tujuan",
                            "alasan",
                        ),
                    },
                ),
                ("Identitas PKBM", {"fields": ()}),
                (
                    "Idenditas Orang Tua",
                    {
                        "fields": (
                            "nomor_nik_ayah",
                            "nama_ayah",
                            "nomor_nik_ibu",
                            "nama_ibu",
                        ),
                    },
                ),
            )
        return fieldsets

    def semua_pengaduan(self, request):
        extra_context = {"title": "Semua Pengaduan", "show_add": True}
        return super().changelist_view(request, extra_context)

    def pengaduan_saya(self, request):
        extra_context = {"title": "Pengaduan Saya", "show_add": True}
        return super().changelist_view(request, extra_context)

    def pengaduan_masuk(self, request):
        extra_context = {"title": "Pengaduan Masuk", "show_add": False}
        return super().changelist_view(request, extra_context)

    def pengaduan_proses(self, request):
        extra_context = {
            "title": "Pengaduan Proses",
            "show_add": False,
            "paket": Pengaduan.Paket.choices,
            "admin_sekolah": request.user.groups.filter(name="Admin Sekolah").exists(),
        }
        return super().changelist_view(request, extra_context)

    def pengaduan_ditolak(self, request):
        extra_context = {"title": "Pengaduan Ditolak", "show_add": False}
        return super().changelist_view(request, extra_context)

    def pengaduan_selesai(self, request):
        extra_context = {"title": "Pengaduan Tertangani Pendidikan", "show_add": False}
        return super().changelist_view(request, extra_context)

    def get_queryset(self, request):
        qs = super(PengaduanAdmin, self).get_queryset(request)
        func_view, func_view_args, func_view_kwargs = resolve(request.path)
        if func_view.__name__ == self.pengaduan_masuk.__name__:
            qs = qs.filter(status=2, is_taken=True)
        elif func_view.__name__ == self.pengaduan_proses.__name__:
            qs = qs.filter(status=5)
        elif func_view.__name__ == self.pengaduan_ditolak.__name__:
            qs = qs.filter(status=4)
        elif func_view.__name__ == self.pengaduan_selesai.__name__:
            qs = qs.filter(status=1)
        elif func_view.__name__ == self.semua_pengaduan.__name__:
            qs = qs.filter(~Q(status__in=[3, 1]))

        if (
            hasattr(request.user, "petugas")
            or hasattr(request.user, "adminsekolah")
            or hasattr(request.user, "masyarakat")
        ):
            if func_view.__name__ == self.semua_pengaduan.__name__:
                qs = qs
            else:
                if not func_view.__name__ == self.pengaduan_saya.__name__:
                    if hasattr(request.user, "petugas"):
                        qs = qs.filter(
                            Q(created_by=request.user)
                            | Q(
                                desa__kecamatan=request.user.petugas.kecamatan_penugasan
                            )
                        )
                    else:
                        qs = qs.filter(
                            Q(created_by=request.user) | Q(taken_by=request.user)
                        )
                else:
                    qs = qs.filter(created_by=request.user)
        return qs

    def change_view(self, request, object_id, form_url="", extra_context={}):
        obj = get_object_or_404(Pengaduan, pk=object_id)
        riwayat_qs = RiwayatPengaduan.objects.filter(pengaduan=obj)
        show_riwayat = False
        if riwayat_qs.exists():
            show_riwayat = True
            riwayat_qs = riwayat_qs.order_by("created_at")
        field = [
            "nama_sekolah",
            "kode_sekolah",
            "kecamatan",
            "desa",
            "alamat",
            "email",
            "status_penyelenggara",
            "kepala_sekolah",
            "operator_sekolah",
            "nomor_hp",
        ]
        extra_context.update(
            {
                "show_riwayat": show_riwayat,
                "riwayat_qs": riwayat_qs,
                "kecamatan_text": obj.desa.kecamatan.__str__() if obj.desa else "---",
                "kecamatan_url": reverse(
                    "admin:master_kecamatan_change",
                    kwargs={"object_id": obj.desa.kecamatan.id},
                )
                if obj.desa
                else "#",
                "field": field,
            }
        )
        if obj.status == 1 or obj.status == 4:
            extra_context.update(
                {
                    "title": "Lihat Aduan {}".format(obj.nomor_aduan),
                    "readonly":True,
                    "no_show_save": False,
                    "show_save_and_add_another": False,
                    "show_delete": False,
                }
            )
        self.request = request
        return super().change_view(
            request,
            object_id,
            form_url,
            extra_context=extra_context,
        )

    def add_view(self, request, form_url="", extra_context={}):
        field = [
            "nama_sekolah",
            "kode_sekolah",
            "kecamatan",
            "desa",
            "alamat",
            "email",
            "status_penyelenggara",
            "kepala_sekolah",
            "operator_sekolah",
            "nomor_hp",
        ]
        extra_context.update({"field": field})
        return self.changeform_view(request, None, form_url, extra_context)

    def get_actions(self, request):
        actions = super().get_actions(request)
        func_view, func_view_args, func_view_kwargs = resolve(request.path)
        if func_view.__name__ == self.pengaduan_masuk.__name__:
            if request.user.is_superuser or hasattr(request.user, "adminsekolah"):
                actions.update(
                    {
                        "proses_act": (
                            proses_act,
                            "proses_act",
                            "Proses Pengaduan yang dipilih",
                        )
                    }
                )
        elif func_view.__name__ == self.pengaduan_proses.__name__:
            if request.user.is_superuser or hasattr(request.user, "adminsekolah"):
                actions.update(
                    {
                        "tolak_act": (
                            tolak_act,
                            "tolak_act",
                            "Tolak Pengaduan yang dipilih",
                        )
                    }
                )
                actions.update(
                    {
                        "setsekolah_act": (
                            setsekolah_act,
                            "setsekolah_act",
                            "Selesai Pengaduan yang dipilih",
                        )
                    }
                )
                # actions.update(
                #     {
                #         "selesai_act": (
                #             selesai_act,
                #             "selesai_act",
                #             "Selesai Pengaduan yang dipilih",
                #         )
                #     }
                # )
        elif (
            func_view.__name__ == self.semua_pengaduan.__name__
            and hasattr(request.user, "adminsekolah")
            or request.user.is_superuser
        ):
            actions.update(
                {
                    "ambil_act": (
                        ambil_act,
                        "ambil_act",
                        "Ambil Pengaduan yang dipilih",
                    )
                }
            )
        elif (
            func_view.__name__ == self.pengaduan_selesai.__name__
            or func_view.__name__ == self.pengaduan_ditolak.__name__
        ):
            actions.update(
                {
                    "kembalikan_pengaduan": (
                        kembalikan_pengaduan,
                        "kembalikan_pengaduan",
                        "Kembalikan Pengaduan yang dipilih",
                    )
                }
            )
        return actions

    def get_riwayat_terakhir(self, obj):
        qs = RiwayatPengaduan.objects.filter(pengaduan=obj)
        if qs.exists():
            qs = qs.last()
            return mark_safe("<nobr>{}</nobr>".format(qs.keterangan))

    get_riwayat_terakhir.short_description = "Riwayat Terakhir"

    def get_waktu(self, obj):
        qs = RiwayatPengaduan.objects.filter(pengaduan=obj)
        if qs.exists():
            qs = qs.last()
            return qs.created_at

    get_waktu.short_description = "Waktu"

    def get_list_display(self, request):
        func_view, func_view_args, func_view_kwargs = resolve(request.path)
        list_display = self.list_display
        if request.user.is_superuser:
            list_display = list_display + ("created_by",)
        if func_view.__name__ == self.pengaduan_ditolak.__name__:
            list_display = list_display + ("keterangan_tolak",)
        elif func_view.__name__ == self.semua_pengaduan.__name__:
            list_display = list_display + ("is_taken", "taken_by")
        elif (
            func_view.__name__ == self.pengaduan_proses.__name__
            or func_view.__name__ == self.pengaduan_selesai.__name__
        ):
            list_display = list_display + ("sekolah", "paket")
        elif func_view.__name__ == self.pengaduan_saya.__name__:
            list_display = list_display + (
                "get_riwayat_terakhir",
                "get_waktu",
                "get_aksi",
            )
        return list_display

    def save_model(self, request, obj, form, change):
        save = super(PengaduanAdmin, self).save_model(request, obj, form, change)
        if change:
            RiwayatPengaduan.objects.create(
                pengaduan=obj,
                keterangan=f"Pengaduan diubah {form.changed_data}",
                created_by=request.user,
            )
        else:
            obj.created_by = request.user
            obj.save()
            RiwayatPengaduan.objects.create(
                pengaduan=obj, keterangan="Pengaduan dibuat", created_by=request.user
            )
        return save

    def response_add(self, request, obj, post_url_continue=None):
        result = super().response_add(request, obj, post_url_continue)
        if hasattr(request.user, "masyarakat"):
            result["location"] = reverse("admin:pengaduan_pengaduan_saya")
        else:
            result["location"] = reverse("admin:pengaduan_semua_pengaduan")
        return result

    def response_change(self, request, obj):
        result = super(PengaduanAdmin, self).response_change(request, obj)
        if obj.status == 2:
            if obj.is_taken:
                result["location"] = reverse("admin:pengaduan_pengaduan_masuk")
            else:
                result["location"] = reverse("admin:pengaduan_semua_pengaduan")
        elif obj.status == 5:
            result["location"] = reverse("admin:pengaduan_pengaduan_proses")
        elif obj.status == 4:
            result["location"] = reverse("admin:pengaduan_pengaduan_ditolak")
        elif obj.status == 1:
            result["location"] = reverse("admin:pengaduan_pengaduan_selesai")
        if hasattr(request.user, "masyarakat"):
            result["location"] = reverse("admin:pengaduan_pengaduan_saya")
        return result

    # def get_list_display_links(self, request, list_display):
    #     link = ("nomor_aduan",)
    #     func_view, func_view_args, func_view_kwargs = resolve(request.path)
    #     if func_view.__name__ == self.pengaduan_selesai.__name__:
    #         link = None
    #     return link

    def form_export_pengaduan(self, request, extra_context={}):
        if request.GET:
            qs = Pengaduan.objects.all().order_by("nama")
            extra_context.update({"title": "Export Pengaduan", "qs": qs})
            return render(
                request, "admin/pengaduan/pengaduan/excel.html", extra_context
            )

        extra_context.update(
            {
                "title": "Export Pengaduan",
            }
        )
        return render(request, "admin/pengaduan/pengaduan/export.html", extra_context)

    def cetak_pernyataan(self, request, id, extra_context={}):
        obj = get_object_or_404(Pengaduan, pk=id)
        kecamatan = "---"
        if obj.desa_domisili:
            kecamatan = obj.desa_domisili.kecamatan.nama
        elif obj.desa:
            kecamatan = obj.desa.kecamatan.nama
        extra_context.update(
            {
                "title": "Cetak Pernyataan {} - {}".format(obj.nama, kecamatan),
                "obj": obj,
                "kecamatan": kecamatan,
            }
        )
        return render(
            request, "admin/pengaduan/pengaduan/cetak_pernyataan.html", extra_context
        )

    def load_pengaduan(self, request):
        pk = request.GET.get("id", None)
        respon = {"success": False}
        if pk:
            obj = get_object_or_404(Pengaduan, pk=pk)
            get_riwayat = RiwayatPengaduan.objects.filter(pengaduan=obj)
            if get_riwayat.exists():
                get_riwayat = get_riwayat.last().keterangan
            else:
                get_riwayat = "---"
            alamat = "---"
            desa = "---"
            kecamatan = "---"
            if obj.alamat_lengkap_domisili:
                alamat = obj.alamat_lengkap_domisili
            elif obj.alamat_lengkap:
                alamat = obj.alamat_lengkap

            if obj.desa_domisili:
                desa = obj.desa_domisili.nama
                kecamatan = obj.desa_domisili.kecamatan.nama
            elif obj.desa:
                desa = obj.desa.nama
                kecamatan = obj.desa.kecamatan.nama

            respon = {
                "success": True,
                "nomor_aduan": obj.nomor_aduan,
                "nik": obj.nomor_nik,
                "nama": obj.nama,
                "ttl": "{}, {}".format(
                    obj.tempat_lahir, obj.tanggal_lahir.strftime("%d-%m-%Y")
                )
                if obj.tanggal_lahir and obj.tempat_lahir
                else "---",
                "jenis_kelamin": obj.get_jenis_kelamin_display(),
                "usia": "{} Tahun".format(obj.usia),
                "pendidikan_terakhir": obj.get_pendidikan_terakhir_display(),
                "kelas": obj.kelas.__str__() if obj.kelas else "---",
                "keterangan": obj.keterangan.nama if obj.keterangan else "---",
                "alamat": alamat,
                "desa": desa,
                "kecamatan": kecamatan,
                "no_hp": obj.nomor_hp,
                "nik_ayah": obj.nomor_nik_ayah,
                "nama_ayah": obj.nama_ayah,
                "nik_ibu": obj.nomor_nik_ibu,
                "nama_ibu": obj.nama_ibu,
                "sekolah": obj.sekolah.__str__() if obj.sekolah else "---",
                "paket": obj.get_paket_display() if obj.paket else "---",
                "riwayat_terakhir": get_riwayat,
            }

        return JsonResponse(respon, safe=False)

    def get_urls(self):
        from django.urls import path

        urls = super().get_urls()
        my_urls = [
            path(
                "semua/",
                self.admin_site.admin_view(self.semua_pengaduan),
                name="pengaduan_semua_pengaduan",
            ),
            path(
                "saya/",
                self.admin_site.admin_view(self.pengaduan_saya),
                name="pengaduan_pengaduan_saya",
            ),
            path(
                "masuk/",
                self.admin_site.admin_view(self.pengaduan_masuk),
                name="pengaduan_pengaduan_masuk",
            ),
            path(
                "proses/",
                self.admin_site.admin_view(self.pengaduan_proses),
                name="pengaduan_pengaduan_proses",
            ),
            path(
                "ditolak/",
                self.admin_site.admin_view(self.pengaduan_ditolak),
                name="pengaduan_pengaduan_ditolak",
            ),
            path(
                "selesai/",
                self.admin_site.admin_view(self.pengaduan_selesai),
                name="pengaduan_pengaduan_selesai",
            ),
            path(
                "export/",
                self.admin_site.admin_view(self.form_export_pengaduan),
                name="pengaduan_pengaduan_export",
            ),
            path(
                "load/",
                self.admin_site.admin_view(self.load_pengaduan),
                name="pengaduan_pengaduan_load",
            ),
            path(
                "<int:id>/cetak-pernyataan/",
                self.admin_site.admin_view(self.cetak_pernyataan),
                name="pengaduan_pengaduan_cetak_pernyataan",
            ),
        ]
        return my_urls + urls


admin.site.register(Pengaduan, PengaduanAdmin)
