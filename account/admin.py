from django import forms
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.safestring import mark_safe

from account.forms import CustomUserChangeForm, CustomUserCreationForm, ProfileForm, ProfileMasyarakatForm, ProfilePetugasForm, ProfileAdminSekolahForm
from account.models import Account, Petugas, AdminSekolah, Masyarakat
from master.models import Kecamatan, Desa


# Register your models here.
class AccountAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = Account
    list_per_page = 15
    list_display = ("username", "nama_lengkap", "is_staff", "is_active")
    list_filter = []
    fieldsets = (
        (None, {"fields": ("username", "nama_lengkap", "password", "nomor_hp")}),
        (
            "Permissions",
            {"fields": ("is_staff", "is_active", "is_superuser", "groups")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "nama_lengkap",
                    "username",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                ),
            },
        ),
    )
    search_fields = ("email", "username", "nama_lengkap")
    ordering = ("email",)

    def changelist_view(self, request, extra_context={}):
        extra_context.update({"tampilan_user": True})
        self.request = request
        return super().changelist_view(request, extra_context)

    def change_password_view(self, request, account_id):
        account_obj = get_object_or_404(Account, pk=account_id)
        extra_context = {"title": "Ganti Password " + account_obj.username}

        if request.POST:
            password1 = request.POST.get("password1", None)
            password2 = request.POST.get("password2", None)

            if password1 and password2:
                if password1 == password2:
                    account_obj.set_password(password1)
                    account_obj.save()
                    messages.success(
                        request,
                        mark_safe(
                            "Berhasil mengganti password "
                            + account_obj.username
                            + "<br>silahkan Login Kembali"
                        ),
                    )
                    return HttpResponseRedirect(reverse("admin:login"))
                else:
                    messages.warning(request, "Password 1 dan Password 2 tidak sama")
            else:
                messages.warning(request, "Password tidak boleh kosong")
        return render(request, "password/change_form.html", extra_context)

    def account_profile(self, request, account_id, extra_context={}):
        obj = get_object_or_404(Account, pk=account_id)
        mode = "account"
        if hasattr(obj, "masyarakat"):
            obj = get_object_or_404(Masyarakat, pk=account_id)
            mode = "masyarakat"
        elif hasattr(obj, "adminsekolah"):
            obj = get_object_or_404(AdminSekolah, pk=account_id)
            mode = "adminsekolah"
        elif hasattr(obj, "petugas"):
            obj = get_object_or_404(Petugas, pk=account_id)
            mode = "petugas"

        if request.POST:
            if mode == "masyarakat":
                forms = ProfileMasyarakatForm(request.POST, instance=obj)
            elif mode == "adminsekolah":
                forms = ProfileAdminSekolahForm(request.POST, instance=obj)
            elif mode == "petugas":
                forms = ProfilePetugasForm(request.POST, instance=obj)
            else:
                forms = ProfileForm(request.POST, instance=obj)
            if forms.is_valid():
                forms.save()
                messages.success(
                    request, "Berhasil Merubah Profil {}".format(obj.nama_lengkap)
                )
            else:
                messages.error(
                    request,
                    mark_safe("Terjadi Kesalahan<br>{}".format(forms.errors.as_ul())),
                )
        else:
            if mode == "masyarakat":
                forms = ProfileMasyarakatForm(instance=obj)
            elif mode == "adminsekolah":
                forms = ProfileAdminSekolahForm(instance=obj)
            elif mode == "petugas":
                forms = ProfilePetugasForm(instance=obj)
            else:
                forms = ProfileForm(instance=obj)

        extra_context.update(
            {"title": f"Profile {obj.nama_lengkap}", "forms": forms, "original": obj}
        )
        return render(request, "admin/account/account/profile.html", extra_context)

    def get_urls(self):
        from django.urls import path

        urls = super().get_urls()
        my_urls = [
            path(
                "<int:account_id>/password/",
                self.admin_site.admin_view(self.change_password_view),
                name="account_account_password",
            ),
            path(
                "<int:account_id>/profile/",
                self.admin_site.admin_view(self.account_profile),
                name="account_account_profile",
            ),
        ]
        return my_urls + urls


# Register your models here.


class PetugasForm(UserChangeForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    kecamatan_penugasan = forms.ModelChoiceField(
        required=True,
        queryset=Kecamatan.objects.all(),
        widget=forms.Select(),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(PetugasForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        # self.fields["is_active"].initial = True
        # self.fields["is_staff"].initial = True
        # self.fields["is_staff"].widget = forms.HiddenInput()
        # self.fields["is_active"].widget = forms.HiddenInput()
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass


class PetugasAdmin(UserAdmin):
    list_display = ("username", "nama_lengkap", "get_kecamatan", "desa")
    form = PetugasForm
    search_fields = ("email", "username", "nama_lengkap")
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "nama_lengkap",
                    "tempat_lahir",
                    "tanggal_lahir",
                    "username",
                    "password1",
                    "password2",
                    # "is_staff",
                    # "is_active",
                ),
            },
        ),
    )
    fieldsets = (
        (None, {"fields": ("username", "nama_lengkap", "password")}),
        ("Penugasan", {"fields": ("kecamatan_penugasan",)}),
        (
            "Profil",
            {
                "fields": (
                    "tempat_lahir",
                    "tanggal_lahir",
                    "kecamatan",
                    "desa",
                    "alamat",
                    "nomor_hp",
                )
            },
        ),
    )

    def get_kecamatan(self, obj):
        if obj.desa:
            return obj.desa.kecamatan
        return "-"

    get_kecamatan.short_description = "Kecamatan"


class MasyarakatForm(UserChangeForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(MasyarakatForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        # self.fields["is_active"].initial = True
        # self.fields["is_staff"].initial = True
        # self.fields["is_staff"].widget = forms.HiddenInput()
        # self.fields["is_active"].widget = forms.HiddenInput()
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass


class MasyarakatCreation(UserCreationForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)
    nik = forms.CharField(max_length=16, widget=forms.NumberInput(), label="NIK Pelapor")

    alamat = forms.CharField(widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(MasyarakatCreation, self).__init__(*args, **kwargs)
        self.fields["is_active"].initial = True
        self.fields["is_staff"].initial = True
        self.fields["is_staff"].widget = forms.HiddenInput()
        self.fields["is_active"].widget = forms.HiddenInput()

        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    class Meta:
        model = Masyarakat
        fields = [
            "nik",
            "username",
            "nama_lengkap",
            "tempat_lahir",
            "tanggal_lahir",
            "nomor_hp",
            "kecamatan",
            "desa",
            "dusun",
            "rt",
            "rw",
            "alamat",
            "password1",
            "password2",
            "is_active",
            "is_staff",
        ]


class MasyarakatAdmin(UserAdmin):
    search_fields = ("email", "username", "nama_lengkap")
    list_display = ("username", "nama_lengkap", "get_kecamatan", "desa")
    form = MasyarakatForm
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "nama_lengkap",
                    "tempat_lahir",
                    "tanggal_lahir",
                    "username",
                    "password1",
                    "password2",
                    # "is_staff",
                    # "is_active",
                ),
            },
        ),
    )
    fieldsets = (
        (None, {"fields": ("username", "nama_lengkap", "password")}),
        (
            "Profil",
            {
                "fields": (
                    "tempat_lahir",
                    "tanggal_lahir",
                    "kecamatan",
                    "desa",
                    "dusun",
                    "rt",
                    "rw",
                    "alamat",
                    "nomor_hp",
                )
            },
        ),
    )

    def get_kecamatan(self, obj):
        if obj.desa:
            return obj.desa.kecamatan
        return "-"

    get_kecamatan.short_description = "Kecamatan"


class AdminSekolahForm(UserChangeForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(AdminSekolahForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass


class AdminSekolahAdmin(UserAdmin):
    search_fields = ("email", "username", "nama_lengkap")
    list_display = ("username", "nama_lengkap", "get_kecamatan", "desa", "sekolah")
    form = AdminSekolahForm
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "nama_lengkap",
                    "sekolah",
                    "tempat_lahir",
                    "tanggal_lahir",
                    "username",
                    "password1",
                    "password2",
                    # "is_staff",
                    # "is_active",
                ),
            },
        ),
    )
    fieldsets = (
        (None, {"fields": ("username", "nama_lengkap", "password")}),
        (
            "Profil",
            {
                "fields": (
                    "sekolah",
                    "tempat_lahir",
                    "tanggal_lahir",
                    "kecamatan",
                    "desa",
                    "alamat",
                    "nomor_hp",
                )
            },
        ),
    )

    def get_kecamatan(self, obj):
        if obj.desa:
            return obj.desa.kecamatan
        return "-"

    get_kecamatan.short_description = "Kecamatan"


admin.site.register(Account, AccountAdmin)
admin.site.register(Petugas, PetugasAdmin)
admin.site.register(AdminSekolah, AdminSekolahAdmin)
admin.site.register(Masyarakat, MasyarakatAdmin)
