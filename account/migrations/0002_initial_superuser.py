import os
from django.db import migrations
from django.core.management import call_command
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.contrib.auth.hashers import make_password

from account.models import Account

username = "admin"
email = "admin@admin.id"
password = "tester"


def generate_superuser(apps, schema_editor):
    User = apps.get_model("account.Account")

    user = User()
    user.username = AbstractBaseUser.normalize_username(username)
    user.nama_lengkap = AbstractBaseUser.normalize_username(username)
    user.email = BaseUserManager.normalize_email(email)
    user.password = make_password(password)
    user.is_staff = True
    user.is_active = True
    user.is_superuser = True
    user.save()


def forwards_func(apps, schema_editor):
    print("forwards")
    generate_superuser(apps, schema_editor)


def reverse_func(apps, schema_editor):
    print("reverse")
    Account.objects.filter(username=username).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("account", "0001_initial"),
    ]
    operations = [migrations.RunPython(forwards_func, reverse_func, elidable=False)]
