# Generated by Django 4.2.1 on 2023-07-22 13:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("account", "0008_account_nomor_hp"),
    ]

    operations = [
        migrations.AddField(
            model_name="masyarakat",
            name="dusun",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Dusun"
            ),
        ),
        migrations.AddField(
            model_name="masyarakat",
            name="nik",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="NIK Pelapor"
            ),
        ),
        migrations.AddField(
            model_name="masyarakat",
            name="rt",
            field=models.IntegerField(default=0, null=True, verbose_name="RT"),
        ),
        migrations.AddField(
            model_name="masyarakat",
            name="rw",
            field=models.IntegerField(default=0, null=True, verbose_name="RW"),
        ),
    ]
