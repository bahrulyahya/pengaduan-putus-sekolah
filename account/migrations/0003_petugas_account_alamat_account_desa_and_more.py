# Generated by Django 4.2.1 on 2023-05-09 14:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("master", "0001_initial"),
        ("account", "0002_initial_superuser"),
    ]

    operations = [
        migrations.CreateModel(
            name="Petugas",
            fields=[
                (
                    "account_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "verbose_name": "Petugas",
                "verbose_name_plural": "Petugas",
                "ordering": ["id"],
            },
            bases=("account.account",),
        ),
        migrations.AddField(
            model_name="account",
            name="alamat",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Alamat"
            ),
        ),
        migrations.AddField(
            model_name="account",
            name="desa",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="master.desa",
            ),
        ),
        migrations.AddField(
            model_name="account",
            name="tanggal_lahir",
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="account",
            name="tempat_lahir",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Tempat Lahir"
            ),
        ),
    ]
