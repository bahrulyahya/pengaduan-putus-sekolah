from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from account.models import Account, Masyarakat, Petugas, AdminSekolah
from django.contrib.auth.models import Group
from master.models import Kecamatan, Desa


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = Account
        fields = ("email",)


class CustomUserChangeForm(UserChangeForm):
    group = forms.ModelMultipleChoiceField(
        label="Menu",
        queryset=Group.objects.all(),
        required=False,
        widget=FilteredSelectMultiple(verbose_name="Menu", is_stacked=False),
    )

    class Meta:
        model = Account
        fields = ("email",)


class ProfileForm(forms.ModelForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    class Meta:
        model = Account
        fields = (
            "nama_lengkap",
            "email",
            "tempat_lahir",
            "tanggal_lahir",
            "kecamatan",
            "desa",
            "alamat",
        )


class ProfileMasyarakatForm(forms.ModelForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)
    nik = forms.CharField(max_length=16, widget=forms.NumberInput(), label="NIK Pelapor")

    alamat = forms.CharField(widget=forms.Textarea(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(ProfileMasyarakatForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    class Meta:
        model = Masyarakat
        fields = (
            "nik",
            "username",
            "nama_lengkap",
            "tempat_lahir",
            "tanggal_lahir",
            "nomor_hp",
            "kecamatan",
            "desa",
            "dusun",
            "rt",
            "rw",
            "alamat",
        )


class ProfileAdminSekolahForm(forms.ModelForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    alamat = forms.CharField(widget=forms.Textarea() ,required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(ProfileAdminSekolahForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    class Meta:
        model = AdminSekolah
        fields = (
            "username",
            "nama_lengkap",
            "sekolah",
            "tempat_lahir",
            "tanggal_lahir",
            "kecamatan",
            "desa",
            "alamat",
            "nomor_hp",
        )


class ProfilePetugasForm(forms.ModelForm):
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    alamat = forms.CharField(widget=forms.Textarea(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(ProfilePetugasForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    class Meta:
        model = Petugas
        fields = (
            "username",
            "nama_lengkap",
            "tempat_lahir",
            "tanggal_lahir",
            "kecamatan",
            "desa",
            "alamat",
            "nomor_hp",
        )
