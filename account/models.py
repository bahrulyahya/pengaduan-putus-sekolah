from django.contrib.auth.models import AbstractUser, BaseUserManager, Group
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class AccountManager(BaseUserManager):
    def create_user(self, username, nama_lengkap, password=None):
        """
        Creates and saves a User with the given username, nama_lengkap and password.
        """
        if not username:
            raise ValueError("Users must have an username")
        user = self.model(
            username=username,
            nama_lengkap=nama_lengkap,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, nama_lengkap, password):
        """
        Creates and saves a superuser with the given identity number,
        nama_lengkap and password.
        """
        user = self.create_user(username, password=password, nama_lengkap=nama_lengkap)
        user.is_admin = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


# Create your models here.
class Account(AbstractUser):
    nama_lengkap = models.CharField("Nama Lengkap", max_length=150)
    alamat = models.CharField("Alamat", max_length=255, null=True, blank=True)
    desa = models.ForeignKey(
        "master.Desa", on_delete=models.SET_NULL, null=True, blank=True
    )
    tempat_lahir = models.CharField(
        max_length=255, verbose_name="Tempat Lahir", null=True, blank=True
    )
    tanggal_lahir = models.DateField(null=True, blank=True)

    objects = AccountManager()
    nomor_hp = models.BigIntegerField(default=0)
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["nama_lengkap"]

    def __str__(self):
        return self.nama_lengkap

    class Meta:
        ordering = ["id"]
        verbose_name = "Akun"
        verbose_name_plural = "Akun"


class Petugas(Account):
    kecamatan_penugasan = models.ForeignKey(
        "master.Kecamatan",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Petugas Kecamatan",
    )

    def __str__(self):
        return "Petugas Kecamatan {} - {} - {}".format(
            self.kecamatan_penugasan.nama if self.kecamatan_penugasan else "-",
            self.username,
            self.nama_lengkap,
        )

    class Meta:
        ordering = ["id"]
        verbose_name = "Petugas"
        verbose_name_plural = "Petugas"


@receiver(post_save, sender=Petugas)
def create_petugas_group(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name="Petugas"))
        instance.is_staff = True
        instance.is_active = True
        instance.save()


class AdminSekolah(Account):
    sekolah = models.ForeignKey("master.Sekolah", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "Admin Sekolah {} - {}".format(self.username, self.nama_lengkap)

    class Meta:
        ordering = ["id"]
        verbose_name = "Admin Sekolah"
        verbose_name_plural = "Admin Sekolah"


@receiver(post_save, sender=AdminSekolah)
def create_adminsekolah_group(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name="Admin Sekolah"))
        instance.is_staff = True
        instance.is_active = True
        instance.save()


class Masyarakat(Account):
    nik = models.CharField(max_length=255, null=True, blank=True, verbose_name="NIK Pelapor")
    rt = models.IntegerField(null=True, default=0, verbose_name="RT")
    rw = models.IntegerField(null=True, default=0, verbose_name="RW")
    dusun = models.CharField(max_length=255, null=True, blank=True, verbose_name="Dusun")

    def __str__(self):
        return self.nama_lengkap

    class Meta:
        ordering = ["id"]
        verbose_name = "Masyarakat"
        verbose_name_plural = "Masyarakat"


@receiver(post_save, sender=Masyarakat)
def create_masyarakat_group(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name="Masyarakat"))
        instance.is_staff = True
        instance.is_active = True
        instance.save()
