import requests
from django.core.management.base import BaseCommand

from django.contrib.auth.models import Permission, Group

from account.models import Petugas, AdminSekolah, Masyarakat


class Command(BaseCommand):
    help = "Generate Data Wilayah Kab. Kediri"

    def handle(self, *args, **options):
        self.stdout.write(self.style.NOTICE("Create Petugas"))
        obj_g, created_g = Group.objects.get_or_create(name="Petugas")
        if created_g:
            pass
        else:
            obj_g.permissions.clear()
        obj_g.permissions.add(
            *list(
                Permission.objects.filter(
                    codename__icontains="pengaduan", content_type__model="pengaduan"
                ).values_list("id", flat=True)
            )
        )
        obj_g.save()
        self.stdout.write(self.style.SUCCESS("Selesai Create Petugas"))
        if Petugas.objects.exists():
            self.stdout.write(self.style.NOTICE("Petugas ditemukan, set group Petugas"))
            for idx, i in enumerate(Petugas.objects.all()):
                obj_g.user_set.add(i)
                self.stdout.write(
                    self.style.NOTICE(
                        "{} ({}/{})".format(
                            i.nama_lengkap, idx + 1, Petugas.objects.all().count()
                        )
                    )
                )
            self.stdout.write(self.style.SUCCESS("Selesai set Petugas Group"))

        self.stdout.write(self.style.NOTICE("Create Admin Sekolah"))
        obj_as, created_as = Group.objects.get_or_create(name="Admin Sekolah")
        if created_as:
            pass
        else:
            obj_as.permissions.clear()
        obj_as.permissions.add(
            *list(
                Permission.objects.filter(
                    codename__icontains="pengaduan", content_type__model="pengaduan"
                ).values_list("id", flat=True)
            )
        )
        obj_as.save()
        self.stdout.write(self.style.SUCCESS("Selesai Create Admin Sekolah"))
        if AdminSekolah.objects.exists():
            self.stdout.write(
                self.style.NOTICE("Admin Sekolah ditemukan, set group Admin Sekolah")
            )
            for idx, i in enumerate(AdminSekolah.objects.all()):
                obj_as.user_set.add(i)
                self.stdout.write(
                    self.style.NOTICE(
                        "{} ({}/{})".format(
                            i.nama_lengkap, idx + 1, AdminSekolah.objects.all().count()
                        )
                    )
                )

        self.stdout.write(self.style.NOTICE("Create Masyarakat"))
        obj_m, created_m = Group.objects.get_or_create(name="Masyarakat")
        if created_m:
            pass
        else:
            obj_m.permissions.clear()
        obj_m.permissions.add(
            *list(
                Permission.objects.filter(
                    codename__icontains="pengaduan", content_type__model="pengaduan"
                ).values_list("id", flat=True)
            )
        )
        obj_m.save()
        self.stdout.write(self.style.SUCCESS("Selesai Create Masyarakat"))
        if Masyarakat.objects.exists():
            self.stdout.write(
                self.style.NOTICE("Masyarakat ditemukan, set group Masyarakat")
            )
            for idx, i in enumerate(Masyarakat.objects.all()):
                obj_m.user_set.add(i)
                self.stdout.write(
                    self.style.NOTICE(
                        "{} ({}/{})".format(
                            i.nama_lengkap, idx + 1, Masyarakat.objects.all().count()
                        )
                    )
                )
