from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe

# Create your views here.


@login_required(login_url=reverse_lazy("admin:login"))
def menu_pengguna(request):
    menus = []
    if request.user.is_superuser:
        menus += [
            dict(
                id=1,
                title="Beranda",
                icon="fa fa-home",
                url=reverse("dashboard"),
            ),
            dict(
                id=9,
                title="Master",
                icon="fa fa-database",
                children=[
                    dict(
                        parent=9,
                        id=10,
                        title="Sekolah",
                        icon="fa fa-school",
                        url=reverse("admin:master_sekolah_changelist"),
                    ),
                    dict(
                        parent=9,
                        id=11,
                        title="Keterangan",
                        icon="fa fa-comment-dots",
                        url=reverse("admin:master_masterketerangan_changelist"),
                    ),
                    dict(
                        parent=9,
                        id=12,
                        title="Pengaturan",
                        icon="fa fa-cog",
                        url=reverse("admin:master_pengaturan_changelist"),
                    ),
                ],
            ),
            dict(
                id=2,
                title="Pengaduan",
                icon="fa fa-bullhorn",
                children=[
                    dict(
                        parent=2,
                        id=3,
                        title="Semua Pengaduan",
                        icon="fa fa-shopping-basket",
                        url=reverse("admin:pengaduan_semua_pengaduan"),
                    ),
                    dict(
                        parent=2,
                        id=4,
                        title="Pengaduan Masuk",
                        icon="fa fa-envelope",
                        url=reverse("admin:pengaduan_pengaduan_masuk"),
                    ),
                    dict(
                        parent=2,
                        id=5,
                        title="Pengaduan Proses",
                        icon="fa fa-clock",
                        url=reverse("admin:pengaduan_pengaduan_proses"),
                    ),
                    dict(
                        parent=2,
                        id=6,
                        title="Pengaduan Ditolak",
                        icon="fa fa-times",
                        url=reverse("admin:pengaduan_pengaduan_ditolak"),
                    ),
                    dict(
                        parent=2,
                        id=7,
                        title=mark_safe("Pengaduan Tertangani<br>Pendidikan"),
                        icon="fa fa-check",
                        url=reverse("admin:pengaduan_pengaduan_selesai"),
                    ),
                    dict(
                        parent=2,
                        id=8,
                        title=mark_safe("Export Pengaduan"),
                        icon="fa fa-file-excel",
                        url=reverse("admin:pengaduan_pengaduan_export"),
                    ),
                ],
            ),
            dict(
                id=8,
                title="Petugas",
                icon="fa fa-user-secret",
                url=reverse("admin:account_petugas_changelist"),
            ),
            dict(
                id=9,
                title="Admin Sekolah",
                icon="fa fa-user-graduate",
                url=reverse("admin:account_adminsekolah_changelist"),
            ),
            dict(
                id=10,
                title="Masyarakat",
                icon="fa fa-users",
                url=reverse("admin:account_masyarakat_changelist"),
            ),
        ]
    elif (
        hasattr(request.user, "petugas")
        or hasattr(request.user, "adminsekolah")
        or hasattr(request.user, "masyarakat")
    ):
        menus += [
            dict(
                id=1,
                title="Beranda",
                icon="fa fa-home",
                url=reverse("dashboard"),
            ),
        ]
        if hasattr(request.user, "adminsekolah"):
            menus += [
                dict(
                    id=2,
                    title="Profil Sekolah",
                    icon="fa fa-school",
                    url=reverse(
                        "admin:master_sekolah_profil",
                        kwargs={"id_sekolah": request.user.adminsekolah.sekolah.id},
                    ),
                ),
            ]
        if not hasattr(request.user, "masyarakat"):
            menus += [
                dict(
                    id=3,
                    title="Pengaduan",
                    icon="fa fa-bullhorn",
                    children=[
                        dict(
                            parent=3,
                            id=3,
                            title="Semua Pengaduan",
                            icon="fa fa-shopping-basket",
                            url=reverse("admin:pengaduan_semua_pengaduan"),
                        ),
                        dict(
                            parent=3,
                            id=4,
                            title="Pengaduan Saya",
                            icon="fa fa-file-alt",
                            url=reverse("admin:pengaduan_pengaduan_saya"),
                        ),
                        dict(
                            parent=3,
                            id=5,
                            title="Pengaduan Masuk",
                            icon="fa fa-envelope",
                            url=reverse("admin:pengaduan_pengaduan_masuk"),
                        ),
                        dict(
                            parent=3,
                            id=6,
                            title="Pengaduan Proses",
                            icon="fa fa-clock",
                            url=reverse("admin:pengaduan_pengaduan_proses"),
                        ),
                        dict(
                            parent=3,
                            id=7,
                            title="Pengaduan Ditolak",
                            icon="fa fa-times",
                            url=reverse("admin:pengaduan_pengaduan_ditolak"),
                        ),
                        dict(
                            parent=3,
                            id=8,
                            title=mark_safe("Pengaduan Tertangani<br>Pendidikan"),
                            icon="fa fa-check",
                            url=reverse("admin:pengaduan_pengaduan_selesai"),
                        ),
                    ],
                ),
            ]
        else:
            menus += [
                dict(
                    id=3,
                    title="Pengaduan",
                    icon="fa fa-bullhorn",
                    children=[
                        dict(
                            parent=3,
                            id=4,
                            title="Pengaduan Saya",
                            icon="fa fa-file-alt",
                            url=reverse("admin:pengaduan_pengaduan_saya"),
                        ),
                        dict(
                            parent=3,
                            id=5,
                            title="Pengaduan Masuk",
                            icon="fa fa-envelope",
                            url=reverse("admin:pengaduan_pengaduan_masuk"),
                        ),
                        dict(
                            parent=3,
                            id=6,
                            title="Pengaduan Proses",
                            icon="fa fa-clock",
                            url=reverse("admin:pengaduan_pengaduan_proses"),
                        ),
                        dict(
                            parent=3,
                            id=7,
                            title="Pengaduan Ditolak",
                            icon="fa fa-times",
                            url=reverse("admin:pengaduan_pengaduan_ditolak"),
                        ),
                        dict(
                            parent=3,
                            id=8,
                            title=mark_safe("Pengaduan Tertangani<br>Pendidikan"),
                            icon="fa fa-check",
                            url=reverse("admin:pengaduan_pengaduan_selesai"),
                        ),
                    ],
                ),
            ]

    return JsonResponse(menus, safe=False)


def menu_pengguna_frontend(request):
    menus = []
    menus += [
        dict(
            title="Beranda",
            icon="fa fa-home",
            url=reverse_lazy("frontend"),
        ),
        dict(
            title="Register",
            icon="fa fa-user-plus",
            url=reverse_lazy("register"),
        ),
        dict(
            title="Pencarian Sekolah",
            icon="fa fa-school",
            url=reverse_lazy("sekolah"),
        ),
        dict(
            title="Pengaduan",
            icon="fa fa-file-alt",
            url=reverse_lazy("admin:login"),
        ),
    ]
    return JsonResponse(menus, safe=False)
