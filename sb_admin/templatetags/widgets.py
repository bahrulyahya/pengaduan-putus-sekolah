from django import forms
from django import template

# from django.utils.html import format_html
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name="addcls")
def addcls(field, css):
    if hasattr(field, "as_widget"):
        return field.as_widget(attrs={"class": css})
    else:
        return None


@register.filter(name="atribut")
def atribut(field_, attr_):
    if hasattr(field_, "as_widget"):
        attrs = {}
        attrs_from_str = attr_.split("|")
        for attr in attrs_from_str:
            k_, v_ = attr.split(":")
            attrs.update({k_: v_})
        return field_.as_widget(attrs=attrs)
    else:
        return None


@register.filter("is_select")
def is_select(field):
    if isinstance(field.field.widget, forms.RadioSelect) or isinstance(
        field.field.widget, forms.widgets.CheckboxSelectMultiple
    ):
        return False
    return (
        isinstance(field.field.widget, forms.Select)
        or str(field.field.widget.__class__.__name__) == "RelatedFieldWidgetWrapper"
    )


@register.filter("is_date")
def is_date(field):
    return isinstance(field.field.widget, forms.DateInput)


@register.filter("is_datetime")
def is_datetime(field):
    return isinstance(field.field.widget, forms.SplitDateTimeWidget)


@register.filter("is_time")
def is_time(field):
    return isinstance(field.field.widget, forms.TimeInput)


@register.filter("is_file")
def is_file(field):
    # if "file" in field:
    return isinstance(field.field.widget, forms.FileInput)


@register.filter("is_bool")
def is_bool(field):
    # if "file" in field:
    return isinstance(field.field.widget, forms.CheckboxInput)


@register.filter("is_hidden")
def is_hidden(field):
    # if "file" in field:
    return isinstance(field.field.widget, forms.HiddenInput)


@register.filter("is_readonlypassword")
def is_readonlypassword(field):
    from django.contrib.auth.forms import ReadOnlyPasswordHashWidget

    return isinstance(field.field.widget, ReadOnlyPasswordHashWidget)


@register.filter(name="get_filter_choices")
def get_filter_choices(spec, cl):
    if spec:
        return list(spec.choices(cl))
    else:
        return ()


@register.filter(name="lookup")
def lookup(value, arg):
    return value[arg]


@register.filter(name="get_usulan")
def get_usulan(value):
    split_ = value.split("/")
    remove_empty = [x for x in split_ if x != ""]
    return remove_empty[1]


@register.filter(name="replace")
def replace(value, _str):
    str_split = _str.split("|")
    before = str_split[0]
    after = str_split[1]
    return value.replace(before, after)


@register.filter(name="to_id")
def to_id(value):
    value = value.lower().replace(" ", "_")
    return value


@register.filter(name="format_hp")
def format_hp(value):
    value = str(value)
    if value.startswith("8"):
        return "0" + value
    elif value.startswith("62"):
        return "0" + value.replace(value[0:2], "08", 1)
    return value
