from django import forms
from django.contrib import admin, messages
from django.db.models import Q
from django.http import JsonResponse
from django.urls import path, reverse
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.utils.safestring import mark_safe
from import_export import resources, fields, widgets
from import_export.admin import ImportExportModelAdmin
from import_export.formats import base_formats


from master.models import (
    Desa,
    Kecamatan,
    Sekolah,
    StatusPenyelenggara,
    Kelas,
    Pengaturan,
    MasterKeterangan,
)

from solo.admin import SingletonModelAdmin


class PengaturanAdmin(SingletonModelAdmin):
    def response_add(self, request, obj, post_url_continue=None):
        messages.success(request, ("Ubah Pengaturan Telah Berhasil"))
        return redirect(reverse("admin:master_pengaturan_changelist"))

    def response_change(self, request, obj):
        messages.success(request, ("Ubah Pengaturan Telah Berhasil"))
        return redirect(reverse("admin:master_pengaturan_changelist"))


class KecamatanAdmin(admin.ModelAdmin):
    list_display = ("kode", "nama")
    search_fields = ("nama",)
    readonly_fields = ("created_at", "updated_at")
    fieldsets = (
        (None, {"fields": ("kode", "nama", "status", "created_at", "updated_at")}),
    )
    # autocomplete_fields = ('kabupaten',)
    list_per_page = 30

    def getJson(self, request):
        respon = {"success": False, "pesan": "Terjadi Kesalahan Sistem"}
        # kode_kabupaten = request.GET.get('kode_kabupaten', None)
        q = request.GET.get("q", None)
        data = []
        # if kode_kabupaten:
        # kab_list = Kabupaten.objects.filter(kode=kode_kabupaten)
        # if kab_list.exists():
        # kab_obj = kab_list.last()
        kec_list = Kecamatan.objects.filter(
            # kabupaten=kab_obj,
            status=1
        )
        if q:
            kec_list = kec_list.filter(nama__icontains=q)
        kec_list = kec_list.order_by("nama")
        if kec_list.exists():
            for i in kec_list:
                data.append({"id": i.kode, "text": i.nama.upper()})
        respon = {"success": True, "data": data}
        # else:
        #     respon = {
        #         'success': False,
        #         'pesan': 'Kode Kabupaten Tersebut Tidak Ada'
        #         }
        # else:
        #     respon = {'success': False, 'pesan': "Kode Kabupaten Dibutuhkan"}
        return JsonResponse(respon, safe=False)

    def get_urls(self):
        from django.urls import path

        urls = super().get_urls()
        my_urls = [
            path(
                "getJson/",
                self.getJson,
                name="master_kecamatan_json",
            ),
        ]
        return my_urls + urls


class DesaAdmin(admin.ModelAdmin):
    list_display = ("kode", "nama", "kecamatan")
    search_fields = ("kode", "nama", "kecamatan__nama")
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "kecamatan",
                    "kode",
                    "nama",
                    "status",
                    "created_at",
                    "updated_at",
                )
            },
        ),
    )
    # autocomplete_fields = ('kecamatan',)
    list_per_page = 30
    # form = DesaForm

    def getJson(self, request):
        respon = {"success": False, "pesan": "Terjadi Kesalahan Sistem"}
        kode_kecamatan = request.GET.get("kode_kecamatan", None)
        q = request.GET.get("q", None)
        data = []
        if kode_kecamatan:
            kec_list = Kecamatan.objects.filter(kode=kode_kecamatan)
            if kec_list.exists():
                kec_obj = kec_list.last()
                des_list = Desa.objects.filter(kecamatan=kec_obj, status=1)
                if q:
                    des_list = des_list.filter(nama__icontains=q)
                des_list = des_list.order_by("nama")
                if des_list.exists():
                    for i in des_list:
                        data.append({"id": i.kode, "text": i.nama.upper()})
                respon = {"success": True, "data": data}
            else:
                respon = {
                    "success": False,
                    "pesan": "Kode Kecamatan Tersebut Tidak Ada",
                }
        else:
            respon = {"success": False, "pesan": "Kode Kecamatan Dibutuhkan"}
        return JsonResponse(respon, safe=False)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "getJson/",
                self.getJson,
                name="master_desa_json",
            ),
        ]
        return my_urls + urls


class SekolahForm(forms.ModelForm):
    jumlah_semua_paket = forms.IntegerField(
        required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
    )
    jumlah_total = forms.IntegerField(
        required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
    )
    kecamatan = forms.ModelChoiceField(
        required=False,
        queryset=Kecamatan.objects.none(),
        widget=forms.Select(attrs={"onChange": "getDesa($(this))"}),
    )
    desa = forms.ModelChoiceField(queryset=Desa.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        if not hasattr(self, "request"):
            self.request = kwargs.pop("request", None)
        super(SekolahForm, self).__init__(*args, **kwargs)
        self.fields["kecamatan"].to_field_name = "kode"
        self.fields["desa"].to_field_name = "kode"

        if self.request and not self.request.user.is_superuser:
            self.fields["jumlah_paket_a"] = forms.IntegerField(
                required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
            )
            self.fields["jumlah_paket_b"] = forms.IntegerField(
                required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
            )
            self.fields["jumlah_paket_c"] = forms.IntegerField(
                required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
            )
            self.fields["jumlah_laki_laki"] = forms.IntegerField(
                required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
            )
            self.fields["jumlah_perempuan"] = forms.IntegerField(
                required=False, widget=forms.NumberInput(attrs={"readonly": "readonly"})
            )
        if "kecamatan" in self.data:
            try:
                kec_pk = int(self.data.get("kecamatan"))
                self.fields["kecamatan"].queryset = Kecamatan.objects.filter(
                    kode=kec_pk
                )
            except (ValueError, TypeError):
                pass

        if "desa" in self.data:
            try:
                des_pk = int(self.data.get("desa"))
                self.fields["desa"].queryset = Desa.objects.filter(kode=des_pk)
            except (ValueError, TypeError):
                pass

    field_order = [
        "nomor_urut",
        "kode",
        "nama_sekolah",
        "foto_sekolah",
        "background_header",
        "status_penyelenggara",
        "email_sekolah",
        "nomor_hp_sekolah",
        "kecamatan",
        "desa",
        "alamat_sekolah",
        "jumlah_paket_a",
        "jumlah_paket_b",
        "jumlah_paket_c",
        "jumlah_semua_paket",
        "jumlah_laki_laki",
        "jumlah_perempuan",
        "jumlah_total",
        "jumlah_tutor",
    ]

    class Meta:
        model = Sekolah
        exclude = ("status", "created_by", "created_at", "updated_at")


class SekolahResource(resources.ModelResource):
    kecamatan = fields.Field(
        column_name="kecamatan",
        attribute="kecamatan",
        widget=widgets.ForeignKeyWidget(Kecamatan, "nama"),
    )

    class Meta:
        model = Sekolah
        use_natural_foreign_keys = True
        skip_unchanged = True
        report_skipped = True
        import_id_fields = (
            # "kode_unit_kerja",
            # "kode",
            "nama_sekolah",
            # "status",
        )
        fields = (
            "nama_sekolah",
            "kode",
            "kecamatan",
        )
        export_order = (
            "kode",
            "nama_sekolah",
            "kecamatan",
        )

    def before_import_row(self, row, **kwargs):
        post_kecamatan = row.get("kecamatan")
        if post_kecamatan and post_kecamatan != "":
            qs = Kecamatan.objects.filter(nama__icontains=post_kecamatan)
            if qs:
                obj = qs.last()
                row["kecamatan"] = obj.nama
            else:
                row["kecamatan"] = None


class SekolahAdmin(ImportExportModelAdmin):
    search_fields = ("kode", "nama_sekolah")
    list_display = ("kode", "nama_sekolah", "status_penyelenggara")
    form = SekolahForm
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    resource_class = SekolahResource
    formats = (base_formats.XLSX,)
    change_list_template = "admin/master/sekolah/change_list.html"
    fieldsets = (
        (
            "Identitas Sekolah",
            {
                "fields": (
                    "kode",
                    "nama_sekolah",
                    "foto_sekolah",
                    "background_header",
                    "status_penyelenggara",
                    "email_sekolah",
                    "nomor_hp_sekolah",
                    "kecamatan",
                    "desa",
                    "alamat_sekolah",
                )
            },
        ),
        (
            "Lain - Lain",
            {
                "fields": (
                    "jumlah_paket_a",
                    "jumlah_paket_b",
                    "jumlah_paket_c",
                    "jumlah_semua_paket",
                    "jumlah_laki_laki",
                    "jumlah_perempuan",
                    "jumlah_total",
                    "jumlah_tutor",
                )
            },
        ),
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super(SekolahAdmin, self).get_form(request, obj, **kwargs)
        form.request = request
        return form

    def get_fieldsets(self, request, obj):
        fieldset = super().get_fieldsets(request, obj)
        no_urut = (
            (
                None,
                {"fields": ("nomor_urut",)},
            ),
        )
        if request.user.is_superuser:
            fieldset = no_urut + fieldset
        return fieldset

    def profile(self, request, id_sekolah, extra_context={}):
        obj = get_object_or_404(Sekolah, pk=id_sekolah)
        form = SekolahForm(instance=obj)
        form.fields["nomor_urut"].widget = forms.HiddenInput()
        if request.POST:
            form = SekolahForm(request.POST, request.FILES, instance=obj)
            form.fields["nomor_urut"].widget = forms.HiddenInput()
            if form.is_valid():
                form.save()
                messages.success(
                    request,
                    "Berhasil Merubah Profil Sekolah {}".format(obj.nama_sekolah),
                )
                return redirect(
                    reverse(
                        "admin:master_sekolah_profil",
                        kwargs={"id_sekolah": obj.id},
                    ),
                )
            else:
                messages.error(
                    request,
                    mark_safe("Terjadi Kesalahan<br>{}".format(form.errors.as_ul())),
                )
        extra_context.update(
            {
                "title": "Profil Sekolah - {}".format(obj.nama_sekolah),
                "forms": form,
                "original": obj,
            }
        )
        return render(request, "admin/master/sekolah/profil.html", extra_context)

    def getJson(self, request):
        respon = {"success": False, "pesan": "Terjadi Kesalahan Sistem"}
        q = request.GET.get("q", None)
        kecamatan = request.GET.get("kecamatan", None)
        data = []
        sek_list = Sekolah.objects.all()
        if q:
            sek_list = sek_list.filter(
                Q(nama_sekolah__icontains=q) | Q(kode__icontains=q)
            )
        if kecamatan and kecamatan != "":
            sek_list = sek_list.filter(kecamatan__kode=kecamatan)
        sek_list = sek_list.order_by("nama_sekolah")
        print(sek_list)
        if sek_list.exists():
            for i in sek_list:
                data.append(
                    {
                        "id": i.id,
                        "text": i.nama_sekolah.upper(),
                        "profil": i.foto_sekolah.url if i.foto_sekolah else None,
                        "kode_sekolah": i.kode,
                    }
                )
        respon = {"success": True, "data": data}
        return JsonResponse(respon, safe=False)

    def getJsonFrontend(self, request):
        pk = request.GET.get("pk", None)
        obj = Sekolah.objects.filter(id=pk)
        if obj.exists():
            obj = obj.last()
            return JsonResponse(obj.json_profil(), safe=False)
        return JsonResponse(dict(), safe=False)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("<int:id_sekolah>/profil", self.profile, name="master_sekolah_profil"),
            path("json", self.getJson, name="master_sekolah_json"),
            path(
                "json-frontend",
                self.getJsonFrontend,
                name="master_sekolah_json_frontend",
            ),
        ]
        return my_urls + urls


class StatusPenyelenggaraAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "nama",
                    "status",
                )
            },
        ),
    )


class MasterKeteranganAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "nama",
                    "status",
                )
            },
        ),
    )


class KelasAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "jenjang",
                    "kelas",
                    "status",
                )
            },
        ),
    )

    def get_kelas(self, request):
        jenjang = request.GET.get("jenjang", None)
        respon = {}
        if jenjang:
            data = []
            qs = Kelas.objects.filter(jenjang=jenjang, status=1)
            if qs.exists():
                for i in qs:
                    data.append({"id": i.id, "text": "Kelas {}".format(i.kelas)})
                respon = {"success": True, "data": data}
            else:
                respon = {"success": False, "data": []}
        return JsonResponse(respon, safe=False)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "json",
                self.admin_site.admin_view(self.get_kelas),
                name="master_kelas_json",
            ),
        ]
        return my_urls + urls


admin.site.register(Kecamatan, KecamatanAdmin)
admin.site.register(Desa, DesaAdmin)
admin.site.register(Sekolah, SekolahAdmin)
admin.site.register(StatusPenyelenggara, StatusPenyelenggaraAdmin)
admin.site.register(Kelas, KelasAdmin)
admin.site.register(Pengaturan, PengaturanAdmin)
admin.site.register(MasterKeterangan, MasterKeteranganAdmin)
