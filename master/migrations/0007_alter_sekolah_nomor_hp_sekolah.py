# Generated by Django 4.2.1 on 2023-06-16 20:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("master", "0006_sekolah_background_header_sekolah_kepala_sekolah_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sekolah",
            name="nomor_hp_sekolah",
            field=models.CharField(
                help_text="Dengan format 6281555555555", max_length=254, null=True
            ),
        ),
    ]
