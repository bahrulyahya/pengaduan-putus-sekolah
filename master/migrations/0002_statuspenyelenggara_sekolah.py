# Generated by Django 4.2.1 on 2023-05-16 01:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import master.utils


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("master", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="StatusPenyelenggara",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "status",
                    models.IntegerField(
                        choices=[
                            (1, "ACTIVE"),
                            (2, "DRAFT"),
                            (3, "VERIFIED"),
                            (4, "REJECTED"),
                            (5, "QUEUE"),
                        ],
                        default=2,
                    ),
                ),
                ("nama", models.CharField(blank=True, max_length=255, null=True)),
                (
                    "created_by",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="%(app_label)s_%(class)s_create_by_user",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="Dibuat Oleh",
                    ),
                ),
            ],
            options={
                "verbose_name": "Status Penyelenggara",
                "verbose_name_plural": "Status Penyelenggara",
            },
        ),
        migrations.CreateModel(
            name="Sekolah",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                (
                    "status",
                    models.IntegerField(
                        choices=[
                            (1, "ACTIVE"),
                            (2, "DRAFT"),
                            (3, "VERIFIED"),
                            (4, "REJECTED"),
                            (5, "QUEUE"),
                        ],
                        default=2,
                    ),
                ),
                ("nomor_urut", models.IntegerField(default=0)),
                (
                    "kode",
                    models.CharField(
                        blank=True,
                        max_length=255,
                        null=True,
                        verbose_name="Kode Sekolah",
                    ),
                ),
                (
                    "nama_sekolah",
                    models.CharField(
                        blank=True,
                        max_length=255,
                        null=True,
                        verbose_name="Nama Sekolah",
                    ),
                ),
                (
                    "foto_sekolah",
                    master.utils.ImageField(
                        blank=True,
                        help_text="Mohon jika mengunggah menggunakan gambar dengan ukuran yang lebih kecil",
                        null=True,
                        upload_to=master.utils.PathForFileModel("foto_sekolah/"),
                        verbose_name="Foto Sekolah",
                    ),
                ),
                (
                    "jumlah_paket_a",
                    models.IntegerField(default=0, verbose_name="Paket A (SD)"),
                ),
                (
                    "jumlah_paket_b",
                    models.IntegerField(default=0, verbose_name="Paket B (SMP/MTs)"),
                ),
                (
                    "jumlah_paket_c",
                    models.IntegerField(
                        default=0, verbose_name="Paket C (SMA/SMK/MAN)"
                    ),
                ),
                (
                    "jumlah_laki_laki",
                    models.IntegerField(default=0, verbose_name="Jumlah Laki - Laki"),
                ),
                (
                    "jumlah_perempuan",
                    models.IntegerField(default=0, verbose_name="Jumlah Perempuan"),
                ),
                (
                    "jumlah_tutor",
                    models.IntegerField(default=0, verbose_name="Jumlah Tutor"),
                ),
                ("email_sekolah", models.EmailField(max_length=254, null=True)),
                ("nomor_hp_sekolah", models.CharField(max_length=254, null=True)),
                (
                    "alamat_sekolah",
                    models.CharField(blank=True, max_length=255, null=True),
                ),
                (
                    "created_by",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="%(app_label)s_%(class)s_create_by_user",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="Dibuat Oleh",
                    ),
                ),
                (
                    "desa",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="master.desa",
                    ),
                ),
                (
                    "status_penyelenggara",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="master.statuspenyelenggara",
                    ),
                ),
            ],
            options={
                "verbose_name": "Sekolah",
                "verbose_name_plural": "Sekolah",
            },
        ),
    ]
