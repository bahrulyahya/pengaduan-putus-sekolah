from django.db import models
from solo.models import SingletonModel
from ckeditor_uploader.fields import RichTextUploadingField


# Create your models here.
from master.utils import AtributTambahan, ImageField, PathForFileModel


# Create your models here.
class Kecamatan(AtributTambahan):
    kode = models.BigIntegerField(unique=True)
    nama = models.CharField(max_length=255)

    def __str__(self):
        return "[{}] {}".format(self.kode, self.nama)

    class Meta:
        verbose_name = "Kecamatan"
        verbose_name_plural = "Kecamatan"


class Desa(AtributTambahan):
    kecamatan = models.ForeignKey(Kecamatan, on_delete=models.CASCADE)
    kode = models.BigIntegerField(unique=True)
    nama = models.CharField(max_length=255)

    def __str__(self):
        return "[{}] {} - {}".format(self.kode, self.nama, self.kecamatan.nama)

    class Meta:
        verbose_name = "Desa"
        verbose_name_plural = "Desa"


class StatusPenyelenggara(AtributTambahan):
    nama = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = "Status Penyelenggara"
        verbose_name_plural = "Status Penyelenggara"


class Kelas(AtributTambahan):
    class Jenjang(models.IntegerChoices):
        SD = 1, "SD Sederajat"
        SLTP = 2, "SMP Sederajat"
        SLTA = 3, "SMA Sederajat"
        DROPSD = 4, "Drop Out SD"
        DROPSLTP = 5, "Drop Out SMP"
        DROPSLTA = 6, "Drop Out SMA"

    jenjang = models.IntegerField(
        choices=Jenjang.choices,
        verbose_name="Jenjang",
        null=True,
        blank=True,
    )
    kelas = models.IntegerField(
        default=0,
        verbose_name="Kelas",
        null=True,
        blank=True,
    )

    def __str__(self):
        return "Kelas {}".format(self.kelas)

    class Meta:
        verbose_name = "Kelas"
        verbose_name_plural = "Kelas"


class MasterKeterangan(AtributTambahan):
    nama = models.CharField(
        verbose_name="Keterangan", null=True, blank=True, max_length=255
    )

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = "Keterangan"
        verbose_name_plural = "Keterangan"


class Sekolah(AtributTambahan):
    nomor_urut = models.IntegerField(default=0)
    kode = models.CharField(
        verbose_name="Kode Sekolah", null=True, blank=True, max_length=255
    )
    nama_sekolah = models.CharField(
        verbose_name="Nama Sekolah", null=True, blank=True, max_length=255
    )
    foto_sekolah = ImageField(
        upload_to=PathForFileModel("foto_sekolah/"),
        null=True,
        blank=True,
        verbose_name="Foto Profil Sekolah",
        help_text="Mohon jika mengunggah menggunakan gambar dengan ukuran yang lebih kecil, ukuran 200x200",
    )
    background_header = ImageField(
        upload_to=PathForFileModel("background_header_sekolah/"),
        null=True,
        blank=True,
        verbose_name="Background Header Sekolah",
        help_text="Mohon jika mengunggah menggunakan gambar dengan ukuran yang lebih kecil, ukuran 1200x450",
    )
    status_penyelenggara = models.ForeignKey(
        StatusPenyelenggara, on_delete=models.SET_NULL, null=True, blank=True
    )
    kepala_sekolah = models.CharField(
        max_length=255, null=True, verbose_name="Nama Kepala Sekolah"
    )
    operator_sekolah = models.CharField(
        max_length=255, null=True, verbose_name="Nama Operator Sekolah"
    )
    jumlah_paket_a = models.IntegerField(verbose_name="Paket A (SD)", default=0)
    jumlah_paket_b = models.IntegerField(verbose_name="Paket B (SMP/MTs)", default=0)
    jumlah_paket_c = models.IntegerField(
        verbose_name="Paket C (SMA/SMK/MAN)", default=0
    )
    jumlah_laki_laki = models.IntegerField(verbose_name="Jumlah Laki - Laki", default=0)
    jumlah_perempuan = models.IntegerField(verbose_name="Jumlah Perempuan", default=0)
    jumlah_tutor = models.IntegerField(verbose_name="Jumlah Tutor", default=0)
    email_sekolah = models.EmailField(max_length=254, null=True)
    nomor_hp_sekolah = models.CharField(
        max_length=254, null=True, help_text="Dengan format 6281555555555"
    )
    kecamatan = models.ForeignKey(Kecamatan, on_delete=models.SET_NULL, null=True)
    desa = models.ForeignKey(Desa, on_delete=models.SET_NULL, null=True)
    alamat_sekolah = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{} - {} - {}".format(
            self.kode, self.nama_sekolah, self.desa.kecamatan.nama if self.desa else "-"
        )

    def save(self, *args, **kwargs):
        self.object_list = Sekolah.objects.order_by("nomor_urut")
        if len(self.object_list) == 0:  # if there are no objects
            self.nomor_urut = 1
        else:
            self.nomor_urut = self.object_list.last().nomor_urut + 1
        super(Sekolah, self).save()

    def json_profil(self):
        from django.templatetags.static import static

        default_profil = static("metronic/src/media/users/default.jpg")
        default_header = static("metronic/src/media/bg/sc-home1-bg.png")
        no_hp = "#"
        if self.nomor_hp_sekolah:
            no_hp = "https://wa.me/{}".format(self.nomor_hp_sekolah)
        return dict(
            kepala_sekolah=self.kepala_sekolah if self.kepala_sekolah else "-",
            operator_sekolah=self.operator_sekolah if self.operator_sekolah else "-",
            kode_sekolah=self.kode,
            nama_sekolah=self.nama_sekolah,
            foto_sekolah=self.foto_sekolah.url if self.foto_sekolah else default_profil,
            background_header=self.background_header.url
            if self.background_header
            else default_header,
            nomor_hp=no_hp,
            profile=dict(
                kecamatan=self.desa.kecamatan.nama if self.desa else "-",
                desa=self.desa.nama if self.desa else "-",
                alamat=self.alamat_sekolah if self.alamat_sekolah else "-",
                email=self.email_sekolah,
                status_penyelenggara=self.status_penyelenggara.nama
                if self.status_penyelenggara
                else None,
            ),
            rekap=dict(
                jumlah_paket_a=self.jumlah_paket_a,
                jumlah_paket_b=self.jumlah_paket_b,
                jumlah_paket_c=self.jumlah_paket_c,
                jumlah_laki_laki=self.jumlah_laki_laki,
                jumlah_perempuan=self.jumlah_perempuan,
                jumlah_tutor=self.jumlah_tutor,
            ),
        )

    class Meta:
        verbose_name = "Sekolah"
        verbose_name_plural = "Sekolah"


class Pengaturan(SingletonModel):
    header_frontend = models.ImageField(
        upload_to=PathForFileModel("header/"),
        null=True,
        blank=True,
        verbose_name="Header Halaman Depan",
        help_text="Mohon jika mengunggah menggunakan gambar dengan ukuran yang lebih kecil",
    )
    isi = RichTextUploadingField(blank=True)

    class Meta:
        verbose_name = "Pengaturan"
        verbose_name_plural = "Pengaturan"
