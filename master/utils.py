import datetime
import os
from uuid import uuid4
from django.db import models
from pathlib import Path
from django.utils.deconstruct import deconstructible

from account.models import Account

STATUS = ((1, "ACTIVE"), (2, "DRAFT"), (3, "VERIFIED"), (4, "REJECTED"), (5, "QUEUE"))


def get_no_aduan(pengaduan):
    from pengaduan.models import Pengaduan

    sekarang = datetime.datetime.now()
    model = Pengaduan
    kode = None
    # if not model.__name__ == "PendaftaranGuru":
    index = (
        "ADUAN" + "01-" + sekarang.strftime("%Y%m%d") + "-" + sekarang.strftime("%H:%M")
    )
    get_last_index = model.objects.filter(nomor_aduan__startswith="ADUAN")
    if get_last_index.exists():
        get_last_index = get_last_index.last()
        nomor_aduan = get_last_index.nomor_aduan.split("-")[0]
        nomor_aduan = str(int(nomor_aduan.replace("ADUAN", "")) + 1)
        if len(nomor_aduan) <= 1:
            nomor_aduan = "0" + nomor_aduan
        kode = (
            "ADUAN"
            + nomor_aduan
            + "-"
            + sekarang.strftime("%Y%m%d")
            + "-"
            + sekarang.strftime("%H:%M")
        )
    else:
        kode = index
    # else:
    #     kode = "PST"
    #     kode += (
    #         str(sekarang.strftime("%d"))
    #         + str(sekarang.strftime("%f")[:1])
    #         + str(sekarang.strftime("%S"))
    #         + str(pendaftaran.id)
    #     )

    return kode


class AtributTambahan(models.Model):
    created_by = models.ForeignKey(
        Account,
        related_name="%(app_label)s_%(class)s_create_by_user",
        verbose_name="Dibuat Oleh",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=STATUS, default=2)

    def save(self, *kwargs, **args):
        if not self.id:
            self.created_at = datetime.datetime.now()
        else:
            self.updated_at = datetime.datetime.now()
        return super().save(*kwargs, **args)

    class Meta:
        abstract = True


@deconstructible
class PathForFileModel(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        # set filename as random string
        filename = "{}.{}".format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)


class ImageField(models.ImageField):
    def save_form_data(self, instance, data):
        if data is not None:
            file = getattr(instance, self.attname)
            if file != data:
                file.delete(save=False)
        super(ImageField, self).save_form_data(instance, data)


class FileField(models.FileField):
    def save_form_data(self, instance, data):
        if data is not None:
            file = getattr(instance, self.attname)
            if file != data:
                file.delete(save=False)
        super(FileField, self).save_form_data(instance, data)


def file_is_exists(path_to_file):
    path_file = Path(path_to_file)
    if path_file.exists():
        return True
    return False
