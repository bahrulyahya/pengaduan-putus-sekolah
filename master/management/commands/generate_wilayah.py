import requests
from django.core.management.base import BaseCommand

from master.models import Desa, Kecamatan  # Provinsi,; Kabupaten,


class Command(BaseCommand):
    help = "Generate Data Wilayah Kab. Kediri"

    def handle(self, *args, **options):
        self.stdout.write(self.style.NOTICE("GET DATA KECAMATAN"))
        url = "https://raw.githubusercontent.com/Yusran103/master_wilayah/main/kediri/kabupaten/kecamatan.csv"
        resp = requests.get(url)
        text = resp.text
        data = text.split("\n")
        self.stdout.write(self.style.NOTICE("PROSES DATA KECAMATAN"))
        for d in data:
            lis = d.split(",")
            if len(lis) > 1:
                kecamatan_list = Kecamatan.objects.filter(kode=lis[0])
                if kecamatan_list.exists():
                    kecamatan_obj = kecamatan_list.last()
                    if kecamatan_obj.status == 1:
                        pass
                    else:
                        kecamatan_obj.status = 1
                        kecamatan_obj.save()
                    # kecamatan_list.update(status=1)
                else:
                    Kecamatan.objects.create(nama=lis[2], kode=lis[0], status=1)
        self.stdout.write(self.style.SUCCESS("SELESAI PROSES DATA KECAMATAN"))

        self.stdout.write(self.style.NOTICE("GET DATA DESA"))
        url = "https://raw.githubusercontent.com/Yusran103/master_wilayah/main/kediri/kabupaten/desa.csv"
        resp = requests.get(url)
        text = resp.text
        data = text.split("\n")
        self.stdout.write(self.style.NOTICE("PROSES DATA DESA"))
        for d in data:
            lis = d.split(",")
            if len(lis) > 1:
                kecamatan = Kecamatan.objects.get(kode=lis[1])

                # c,created = Desa.objects.get_or_create(
                # 	kecamatan =kecamatan,
                # 	nama=lis[2],
                # 	kode=lis[0],
                # 	status=2
                # 	)
                # if created:
                # 	c.status = 1
                # 	c.save()
                desa_list = Desa.objects.filter(kode=lis[0], kecamatan=kecamatan)
                if desa_list.exists():
                    desa_obj = desa_list.last()
                    if desa_obj.status == 1:
                        pass
                    else:
                        desa_obj.status = 1
                        desa_obj.save()
                else:
                    Desa.objects.create(
                        kecamatan=kecamatan, nama=lis[2], kode=lis[0], status=1
                    )
        self.stdout.write(self.style.SUCCESS("SELESAI PROSES DATA DESA"))
        self.stdout.write(self.style.SUCCESS("SELESAI"))
